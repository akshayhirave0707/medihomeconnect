package com.dhirajmore.logindemo.Rest;

import com.dhirajmore.logindemo.Entity.Customer;
import com.dhirajmore.logindemo.Service.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@AllArgsConstructor
public class CustomerController {

    private final CustomerService customerService;

    @PostMapping("/login")
    public ResponseEntity<Customer> login(@RequestBody Map<String, String> credentials) {
        String username = credentials.get("username");
        String password = credentials.get("password");

        Customer customer = customerService.login(username, password);


        if (customer != null) {
            return ResponseEntity.ok(customer);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @PostMapping("/add")
    public Customer addCustomer(@RequestBody Customer customer) {

        System.out.println(customer);
        return customerService.addCustomer(customer);
    }
    @PostMapping("/patRequest")
    public ResponseEntity<Customer> patRequest(@RequestBody Map<String,Long> id) {
        long id1 = id.get("id");
        Customer customer = customerService.patientReq(id1);
        if (customer != null) {
            return ResponseEntity.ok(customer);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }
}
