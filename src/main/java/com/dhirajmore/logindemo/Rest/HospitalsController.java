package com.dhirajmore.logindemo.Rest;

import com.dhirajmore.logindemo.Entity.Hospitals;
import com.dhirajmore.logindemo.Service.HospitalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class HospitalsController {

    @Autowired
    HospitalService hospitalService;

    public HospitalsController(HospitalService hospitalService) {
        this.hospitalService = hospitalService;
    }

    @GetMapping("/hospitalList")
    public List<Hospitals> getHospitals() {
        return hospitalService.nearByHospitals();
    }
}
