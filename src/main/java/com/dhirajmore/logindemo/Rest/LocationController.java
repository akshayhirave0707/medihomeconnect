package com.dhirajmore.logindemo.Rest;

import com.dhirajmore.logindemo.Entity.LocationData;
import com.dhirajmore.logindemo.Service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class LocationController {

    @Autowired
    LocationService locationService;

    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }

    @PostMapping("/getLocation/{customerId}")
    public ResponseEntity<String> saveLocation(@RequestBody LocationData locationData,
                                               @PathVariable("customerId") Long customerId) {

        return locationService.saveLoc(locationData, customerId);
    }
    @GetMapping("/getCoordinates/{customerId}")
    public ResponseEntity<LocationData> getCoordinates(@PathVariable(name = "customerId") Long customerId){
        LocationData locationData = locationService.showCoordinates(customerId);
        System.out.println(locationData.getLatitude());
        if (locationData!= null) {
            return ResponseEntity.ok(locationData);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }
}

//AIzaSyCnRSbg-jfhGBuRlH2-_d6aKMRJ_gbbqgY
