package com.dhirajmore.logindemo.Rest;

import com.dhirajmore.logindemo.Entity.Appointment;
import com.dhirajmore.logindemo.Service.AppointmentService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@AllArgsConstructor
public class AppointmentController {

    private final AppointmentService appointmentService;

    @PostMapping("/sendRequest/{customerId}/{doctorId}")
    public void sendReq(@RequestBody Appointment appointment,
                        @PathVariable(name = "customerId") Long customerId,
                        @PathVariable(name = "doctorId") Long doctorId) {
        appointmentService.saveReq(appointment, customerId, doctorId);
    }

    @GetMapping("/showDoctorWise/{doctorId}")
    public ResponseEntity<List<Appointment>> showDoctorWise(@PathVariable(name = "doctorId") Long doctorId) {
        List<Appointment> appointment = appointmentService.showDoctorWise(doctorId);
        if (appointment != null) {
            return ResponseEntity.ok(appointment);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @GetMapping("/showCustomerWise/{customerId}")
    public ResponseEntity<List<Appointment>> showCustomerWise(@PathVariable(name = "customerId") Long customerId) {
        List<Appointment> appointment = appointmentService.showCustomerWise(customerId);
        if (appointment != null) {
            return ResponseEntity.ok(appointment);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @PostMapping("/confirmAppointment/{customerId}/{doctorId}")
    public ResponseEntity<String> confirmAppointment(@PathVariable(name = "customerId") Long customerId,
                                                     @PathVariable(name = "doctorId") Long doctorId) {
        return appointmentService.confirmAppointment(customerId, doctorId);
    }
}
