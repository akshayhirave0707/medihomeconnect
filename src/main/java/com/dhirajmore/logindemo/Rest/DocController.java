package com.dhirajmore.logindemo.Rest;

import com.dhirajmore.logindemo.Entity.Appointment;
import com.dhirajmore.logindemo.Entity.Doctor;
import com.dhirajmore.logindemo.Service.DoctorsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

import java.util.List;

@RestController
public class DocController {
    @Autowired
    DoctorsService doctorsService;

    public DocController(DoctorsService doctorsService) {
        this.doctorsService = doctorsService;
    }

    @PostMapping("/getDoctors")
    public ResponseEntity<List<Doctor>> getDoc(@RequestBody Map<String,Double> coordinates) {
        Double lat = coordinates.get("latitude");
        Double longi = coordinates.get("longitude");
        System.out.println(lat);

        List<Doctor> doctorList = doctorsService.getDoc(lat,longi);

        if (doctorList != null) {
            System.out.println(doctorList);
            System.out.println("THereeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
            return ResponseEntity.ok(doctorList);
        } else {
            System.out.println("Hereeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        //return doctorsService.getDoc(lat,longi);
    }
    @PostMapping("/docLogin")
    public ResponseEntity<Doctor> getDocLog(@RequestBody Map<String,String> credentials) {
        String contact = credentials.get("mobile");
        String password = credentials.get("password");

        System.out.println(contact);
        System.out.println("contact");


        Doctor doctor = doctorsService.login(contact,password);
        if (doctor != null) {
            return ResponseEntity.ok(doctor);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }
    @GetMapping("/getDoctorInfo/{doctorId}")
    public ResponseEntity<Doctor> showDoctorWise(@PathVariable(name = "doctorId") Long doctorId) {
        Doctor doctor = doctorsService.showDoctorInfo(doctorId);
        if (doctor != null) {
            return ResponseEntity.ok(doctor);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }
}
