package com.dhirajmore.logindemo.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PageController {

    @GetMapping("/dashboard")
    public String dashboard() {

        return "Dashboard";
    }
    @GetMapping("/book")
    public String book() {
        return "book";
    }
    @GetMapping("/myprofile")
    public String profile() {
        return "myprofile";
    }
    @GetMapping("/profile")
    public String myprofile() {
        return "profile";
    }
    @GetMapping("/viewDoc")
    public String viewDoctor() {
        return "ViewDoctor";
    }
    @GetMapping("/searchDoc")
    public String searchDoctor() {
        return "SearchDoctor";
    }
    @GetMapping("/appointment")
    public String appointment() {
        return "AppointmentBooking";
    }
    @GetMapping("/addAddress")
    public String addAddress() {
        return "addAddress";
    }
    @GetMapping("/addressSave")
    public String saveAddress(){
        return "addressSave";
    }
    @GetMapping("/showdoc")
    public String showDoc() {
        return "showdoc";
    }
    @GetMapping("/personal")
    public String personal() {
        return "personal";
    }
    @GetMapping("/addresses")
    public String addresses() {
        return "addresses";
    }
    @GetMapping("/doctorlogin")
    public String docLogin() {
        return "doctorlogin";
    }
    @GetMapping("/doctordashboard")
    public String docDash() {
        return "docdash";
    }
    @GetMapping("/doctorRequest")
    public String doctorReq() {
        return "doctorRequest";
    }
    @GetMapping("/aboutus")
    public String aboutus() {
        return "AboutUs";
    }
    @GetMapping("/pending")
    public String pending() {
        return "PendingReq";
    }
    @GetMapping("/confirmedPage")
    public String confirmedPage() {
        return "confirmedPage";
    }
    @GetMapping("/pendingPage")
    public String pendingPage() {
        return "confirmedPage";
    }
    @GetMapping("/patDetails")
    public String patDetails() {
        return "patDetails";
    }
}
