package com.dhirajmore.logindemo.Controllers;

import com.dhirajmore.logindemo.Entity.Customer;
import com.dhirajmore.logindemo.Service.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@AllArgsConstructor
public class LogInController {

    private final CustomerService customerService;

    @GetMapping("/login")
    public String home() {

        return "login";
    }

    @PostMapping("/signin")
    public Customer login(@RequestParam("username") String username, @RequestParam("password") String password){

        return customerService.login(username,password);
    }
    @GetMapping("/sign")
    public String homepage() {
        return "signup";
    }
}
