package com.dhirajmore.logindemo.Controllers;

import com.dhirajmore.logindemo.Service.DoctorsService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
@AllArgsConstructor
public class DoctorsController {

    private final DoctorsService doctorsService;
    @GetMapping("/getDoctorList")
    public void getDrList() {

    }
}
