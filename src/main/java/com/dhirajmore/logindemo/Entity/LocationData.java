package com.dhirajmore.logindemo.Entity;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "location")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LocationData {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @NotNull
    private int id;
    private Double latitude;
    private Double longitude;

    @ManyToOne
    @JoinColumn(
            name = "customer_id",
            referencedColumnName = "customerId",
            foreignKey = @ForeignKey(
                    name = "location_customer_fk"
            )
    )
    private Customer customer;

    public LocationData(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
