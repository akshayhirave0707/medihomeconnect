package com.dhirajmore.logindemo.Entity;

import lombok.*;
import org.springframework.lang.NonNull;

@AllArgsConstructor
@Getter
@Setter
@ToString
@NoArgsConstructor
public class LogIn {
    private String username;
    private String password;

}
