package com.dhirajmore.logindemo.Entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = "doctors_list")
@NoArgsConstructor
public class Doctor {

    @Id
    private Long doctorId;
    private String name;
    private Double lat;
    private Double longi;
    private String contact;
    private Long exp;
    private String education;
    private String spec;

    public Doctor(String name, Double lat, Double longi, String contact, Long exp, String education, String spec) {
        this.name = name;
        this.lat = lat;
        this.longi = longi;
        this.contact = contact;
        this.exp = exp;
        this.education = education;
        this.spec = spec;
    }
}
