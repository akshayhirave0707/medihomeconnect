package com.dhirajmore.logindemo.Entity;

import lombok.*;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "appointment")
@Entity
@ToString
public class Appointment {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long appointmentId;

    @ManyToOne
    @JoinColumn(
            name = "customer_id",
            referencedColumnName = "customerId",
            foreignKey = @ForeignKey(
                    name = "customer_appointment_fk"
            )
    )
    private Customer customer;

    @ManyToOne
    @JoinColumn(
            name = "doctor_id",
            referencedColumnName = "doctorId",
            foreignKey = @ForeignKey(
                    name = "doctor_appointment_fk"
            )
    )
    private Doctor doctor;
    private String address;
    private String pincode;
    private String city;
    private String fullName;
    private String mobile;

    private Long appointmentStatus = 0L;

    public Appointment(String address, String pincode, String city, String fullName, String mobile, Long appointmentStatus) {
        this.address = address;
        this.pincode = pincode;
        this.city = city;
        this.fullName = fullName;
        this.mobile = mobile;
        this.appointmentStatus = appointmentStatus;
    }
}



