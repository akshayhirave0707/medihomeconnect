package com.dhirajmore.logindemo.Repo;

import com.dhirajmore.logindemo.Entity.Appointment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AppointmentRepo extends JpaRepository<Appointment,Long> {

    @Query(value = "select a from Appointment a where a.doctor.doctorId = ?1")
    List<Appointment> findByDoctor(Long doctorId);

    @Query(value = "select a from Appointment a where a.customer.customerId = ?1 and a.doctor.doctorId = ?2")
    Appointment findByCustomerAndDoctor(Long customerId, Long doctorId);

    @Query(value = "select a from Appointment a where a.customer.customerId = ?1")

    List<Appointment> findByCustomer(Long doctorId);
}
