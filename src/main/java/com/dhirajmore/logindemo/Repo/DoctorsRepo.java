package com.dhirajmore.logindemo.Repo;

import com.dhirajmore.logindemo.Entity.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DoctorsRepo extends JpaRepository<Doctor,Long> {

    List<Doctor> findAll();
    @Query(value = "select a from Appointment a where a.doctor.doctorId = ?1")
    Doctor findDoctorById(Long doctorId);
    Doctor findByContact(String contact);
}



























