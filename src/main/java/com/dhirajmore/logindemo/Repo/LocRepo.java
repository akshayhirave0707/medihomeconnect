package com.dhirajmore.logindemo.Repo;

import com.dhirajmore.logindemo.Entity.Doctor;
import com.dhirajmore.logindemo.Entity.LocationData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface LocRepo extends JpaRepository<LocationData,Long> {
    @Query(value = "select a from LocationData a where a.customer.customerId = ?1")
    LocationData findLocationById(Long customerId);
}
