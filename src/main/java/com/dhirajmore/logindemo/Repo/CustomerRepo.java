package com.dhirajmore.logindemo.Repo;

import com.dhirajmore.logindemo.Entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepo extends JpaRepository<Customer,Long> {

    @Query(value = "select c from Customer c where c.email = ?1")
    Customer findByEmail(String email);

    Customer findById(long id);

}
