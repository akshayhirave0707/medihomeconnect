package com.dhirajmore.logindemo.Service;

import com.dhirajmore.logindemo.Entity.Doctor;
import com.dhirajmore.logindemo.Repo.DoctorsRepo;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.print.Doc;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class DoctorsService {

    @Autowired
    private final DoctorsRepo doctorsRepo;


    public List<Doctor> getDoc(Double lat, Double longi) {
        Point2D.Double endCoordinates = calculateCoordinates(lat,longi,10);
        List<Doctor> temp = doctorsRepo.findAll();

        List<Doctor> doctorList = new ArrayList<Doctor>();

        for(int i=0 ;i< temp.size(); i++) {
            System.out.println("Exception");
            if(lat <= temp.get(i).getLat() && temp.get(i).getLat() <= endCoordinates.x ) {
                if(longi <= temp.get(i).getLongi() && temp.get(i).getLongi() <= endCoordinates.y ) {
                    doctorList.add(temp.get(i));
                }
            }
        }
        return doctorList;
    }
    public static Point2D.Double calculateCoordinates(Double latitude, Double longitude, double distanceKm) {
        // Earth radius in kilometers
        final double earthRadiusKm = 6371.0;

        // Convert latitude and longitude from degrees to radians
        double lat1 = Math.toRadians(latitude);
        double lon1 = Math.toRadians(longitude);

        // Calculate the angular distance covered by the distanceKm
        double angularDistance = distanceKm / earthRadiusKm;

        // Calculate new latitude and longitude
        double lat2 = lat1 + angularDistance;
        double lon2 = lon1 + angularDistance / Math.cos(lat1);

        // Convert back to degrees
        lat2 = Math.toDegrees(lat2);
        lon2 = Math.toDegrees(lon2);

        return new Point2D.Double(lat2, lon2);
    }
    public Doctor login(String contact, String password) {
        Doctor doctor = doctorsRepo.findByContact(contact);

        if(password.equals("123"))
            return doctor;
        return null;
    }

    public Doctor showDoctorInfo(Long doctorId) {
        Doctor doctor = doctorsRepo.findDoctorById(doctorId);
        if(doctor != null)
            return doctor;
        return null;
    }
}
