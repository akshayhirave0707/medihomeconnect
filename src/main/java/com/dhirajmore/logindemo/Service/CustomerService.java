package com.dhirajmore.logindemo.Service;

import com.dhirajmore.logindemo.Entity.Customer;
import com.dhirajmore.logindemo.Repo.CustomerRepo;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CustomerService {

    private final CustomerRepo customerRepo;

    public Customer addCustomer(Customer customer) {
      if(customerRepo.findByEmail(customer.getEmail()) != null) {
          return null;
      }
      customerRepo.save(customer);
      return customer;
    }

    public Customer login(String username, String password) {
        System.out.println("hello");
        Customer customer = customerRepo.findByEmail(username);

        if(customer == null) return null;

        if(customer.getPassword().equals(password)) {
            return customer;
        }
        return null;
    }


    public Customer patientReq(long id1) {
        Customer customer = customerRepo.findById(id1);
        if(customer != null)
            return customer;
        return null;
    }
}
