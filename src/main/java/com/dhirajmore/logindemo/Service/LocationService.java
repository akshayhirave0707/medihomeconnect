package com.dhirajmore.logindemo.Service;

import com.dhirajmore.logindemo.Entity.Customer;
import com.dhirajmore.logindemo.Entity.LocationData;
import com.dhirajmore.logindemo.Repo.CustomerRepo;
import com.dhirajmore.logindemo.Repo.LocRepo;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class LocationService {

    private final LocRepo locRepo;
    private final CustomerRepo customerRepo;

    public ResponseEntity<String> saveLoc(LocationData locationData, Long customerId) {

        Customer customer = customerRepo.findById(customerId).get();
        locationData.setCustomer(customer);
        locRepo.save(locationData);
        return ResponseEntity.ok("Location Saved Successfully");
    }

    public LocationData showCoordinates(Long customerId) {
        LocationData locationData = locRepo.findLocationById(customerId);
        System.out.println(locationData);
        if(locationData != null)
            return locationData;
        return null;
    }
}
