package com.dhirajmore.logindemo.Service;

import com.dhirajmore.logindemo.Entity.Hospitals;
import com.dhirajmore.logindemo.Repo.HospitalRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class HospitalService {


    @Autowired
    HospitalRepo hospitalRepo;

    public HospitalService(HospitalRepo hospitalRepo) {
        this.hospitalRepo = hospitalRepo;
    }
    public List<Hospitals> nearByHospitals() {
        List<Hospitals> hospitalList = hospitalRepo.findByPincode(414005);
        hospitalList.addAll(hospitalRepo.findByPincode(414005+1));
        hospitalList.addAll(hospitalRepo.findByPincode(414005+2));
        hospitalList.addAll(hospitalRepo.findByPincode(414005-1));
        hospitalList.addAll(hospitalRepo.findByPincode(414005-2));

        System.out.println(hospitalList);
        return hospitalList;
    }
}
