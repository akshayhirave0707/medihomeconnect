package com.dhirajmore.logindemo.Service;

import com.dhirajmore.logindemo.Entity.Appointment;
import com.dhirajmore.logindemo.Entity.Customer;
import com.dhirajmore.logindemo.Entity.Doctor;
import com.dhirajmore.logindemo.Repo.AppointmentRepo;
import com.dhirajmore.logindemo.Repo.CustomerRepo;
import com.dhirajmore.logindemo.Repo.DoctorsRepo;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class AppointmentService {

    private final AppointmentRepo appointmentRepo;
    private final CustomerRepo customerRepo;
    private final DoctorsRepo doctorsRepo;

    public void saveReq(Appointment appointment, Long customerId, Long doctorId) {
        Customer customer = customerRepo.findById(customerId).get();
        Doctor doctor = doctorsRepo.findById(doctorId).get();
        appointment.setCustomer(customer);
        appointment.setDoctor(doctor);
        System.out.println("saveddddddddddddddddddddddddddddddd");
        appointmentRepo.save(appointment);
    }

    public List<Appointment> showDoctorWise(Long doctorId) {
        List<Appointment> appointment = appointmentRepo.findByDoctor(doctorId);
        if(appointment != null)
            return appointment;
        return null;
    }

    public ResponseEntity<String> confirmAppointment(Long customerId, Long doctorId) {

        Appointment appointment = appointmentRepo.findByCustomerAndDoctor(customerId, doctorId);
        appointment.setAppointmentStatus(1L);
        appointmentRepo.save(appointment);
        return ResponseEntity.ok("Appointment Confirmed");
    }

    public List<Appointment> showCustomerWise(Long customerId) {

        List<Appointment> appointment = appointmentRepo.findByCustomer(customerId);
        if(appointment != null)
            return appointment;
        return null;
    }
}
