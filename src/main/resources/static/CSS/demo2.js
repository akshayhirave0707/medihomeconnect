    let doctors = JSON.parse(sessionStorage.getItem("doctor"));
    console.log(doctors);



    // Assume you have fetched a list of doctors and stored them in the 'doctors' variable

    // Function to dynamically create HTML elements for each doctor
    function displayDoctors() {
        let doctorsContainer = document.getElementById("doctorsContainer");

        doctors.forEach(function (doctor) {
            let doctorBox = document.createElement("div");
            doctorBox.classList.add("box");

            doctorBox.innerHTML = `
                <i class="fas fa-user-md"></i><br>
                <input type="text" class="input" value="${doctor.name}"><br>
                <input type="text" class="input" value="${doctor.education}"><br>
                <input type="text" class="input" value="${doctor.exp}"><br>
<!--                <p>${doctor.description}</p>-->
                <br><a href="/viewDoc" class="btn">Show details <span class="fas fa-chevron-right"></span></a><br><br>
                <br><a href="/addAddress" class="btn" onclick="sendReq('${doctor.contact}')">Book Appointment <span class="fas fa-chevron-right"></span></a>
            `;

            doctorsContainer.appendChild(doctorBox);
        });
    }




    // Call the function to display doctors when the page loads
    displayDoctors();











   let user = JSON.parse(sessionStorage.getItem("user"));
    // Function to send appointment request
    function sendReq(contact) {

        let address = JSON.parse(sessionStorage.getItem("address"));
        var name = address.name;
        var mobile = address.mobile;
        var pincode = address.pincode;
        var city = address.city;
        var addr = address.addr;


        let userId = user.id;
        let credentials = {
            id: userId,
            contact: contact,
            status: 0
//            name:name,
//            mobile:mobile,
//            pincode:pincode,
//            city:city,
//            addr:addr
        };

        fetch('http://localhost:8080/sendRequest', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(credentials)
        })
        .then(response => response.json())
        .then(data => {
            console.log('Server response:', data);
        })
        .catch(error => {
            console.error('Error:', error);
        });

        console.log("Appointment request sent to doctor with contact:", contact);
    }